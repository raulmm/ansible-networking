# ansible-networking

Some ansible Roles to integrate with network devices.

Roles
- manage_vlans ( https://gitlab.com/raulmm/ansible-networking/tree/master/roles/manage_vlans )
  Creates or deletes VLANs.
- backup_config ( https://gitlab.com/raulmm/ansible-networking/tree/master/roles/backup_config )
  Backups the network device configuration.
- compare_and_alert ( https://gitlab.com/raulmm/ansible-networking/tree/master/roles/compare_and_alert )
  Compares the new file with the latest one, if they are the same it doesn nothing but if they differ it:
  * optionally sends and email alert
  * creates an entry in a mysql database
  * updates the webserver with the newest version of the object.
- get_network_information ( https://gitlab.com/raulmm/ansible-networking/tree/master/roles/get_network_information )
  Generic role that depending on the variables passed will collect the specified information and save it to a file.

