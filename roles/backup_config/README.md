backup_config
=========

Backups the network device configuration.

Requirements
------------

INPUT variables are mandatory.

Role Variables
--------------

INPUT
- storage_server:
  This contains the server that will host the output files.

- mygroup:
  This variable indicates which group of hosts are being targeted, it has to be an existing group in the inventory of IOS network devices.

DEFAULTS:
- out_default_loc:
  This contains the location where the files containing the command output will be stored.


Dependencies
------------

This is meant to work with the role "compare_and_alert".

Example Playbook
----------------

```YAML

```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
