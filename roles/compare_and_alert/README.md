compare_and_alert
=========

Compares the new file with the latest one, if they are the same it doesn nothing but if they differ it:
- optionally sends and email alert
- creates an entry in a mysql database
- updates the webserver with the newest version of the object.


Requirements
------------

This is meant to be executed after "get_network_information" role.

Role Variables
--------------

INPUT
- itm_loc:
  This contains the location in the webserver where the files will be placed..
- itm_file_type:
  This defines the type of data, is used in the construction of the filename and the email subject and content (ie. -"{{ itm_file_type }} changes for {{ inventory_hostname }}").
- vault_smtp_pwd:
  Contains the password for the SMTP, this is taken from the vault, if you need to change it, change it in the vault.
- vault_mysql_pwd:
  Contains the password for the MySQL database user, this is taken from the vault, if you need to change it, change it in the vault.
- storage_server:
  This contains the server that will host the output files.

VARIABLES:
- ansible_support_email:
  Contains the default email to be used as sender.
- email_rcpts:
  Contains a list of emails used as recepients of the alerts.
- smtp_pwd:
  References vault_smtp_pwd.
- smtp_usr:
  Contains the SMTP server username.
- smtp_srv:
  Contains the SMTP server address.
- mysql_user:
  Contains the MySQL/Mariadb username
- mysql_host:
  Contains the MySQL/Mariadb server name.
- mysql_pwd:
  References vault_mysql_pwd.
- mysql_dbname:
  Contains the database name.
- mail_enabled:
  This is enabled by default, indicates if there should be email alerts when changes occur.
- web_server_uri:
  Contains the web server URI
- out_default_loc:
  This contains the location where temporary files are located.

Dependencies
------------

This is meant to work with role "get_network_information"

Example Playbook
----------------
```YAML
---
- name: Get information
  hosts: all
  connection: network_cli
  gather_facts: no
  pre_tasks:
    - name: get the date and time
      setup:
        gather_subset: "!all"
      delegate_to: "localhost"
      become: no
    - name: Set the date
      set_fact:
        mydate: "{{ ansible_date_time['epoch'] }}"
      delegate_to: "localhost"
      become: no
  roles:
# We use roles to load the vaults so that variables can be overwriten as they will have the second lowest priority.
    - vault_smtp
    - vault_mysql
    - role: get_network_information
      vars:
        mycommand: 'show ip route'
        mygroup: routers
        info_type: routing-table
        storage_server:  "{{ groups['backup_servers'][0] }}"
    - role: compare_and_alert
      vars:
        itm_loc: "/var/www/html/{{ info_type }}"
        itm_file_type: "{{ info_type }}"
```

License
-------

GPLv3

Author Information
------------------

Raul Mahiques
