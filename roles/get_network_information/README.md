get_network_information
=========
Generic role that depending on the variables passed will collect the specified information and save it to a file.


Requirements
------------
INPUT variables are mandatory.


Role Variables
--------------
INPUT
- mycommand: 
  This variable contains the command to run on the network appliance, the resulting output will be stored into a file that can be later processed.

- mygroup:
  This variable indicates which group of hosts are being targeted, it has to be an existing group in the inventory of IOS network devices.

- info_type:
  This indicates the type of data we are collectin, it has to be a meaningful name with characters supported by the filesystem as it will be used to create directories.

- mydate:
  This variable contains the date in epoch format.

- storage_server:
  This contains the server that will host the output files.

DEFAULTS:
- out_default_loc:
  This contains the location where the files containing the command output will be stored.

Dependencies
------------
This is meant to work with the role "compare_and_alert".


Example Playbook
----------------
```YAML
---
- name: Get information
  hosts: all
  connection: network_cli
  gather_facts: no
  pre_tasks:
    - name: get the date and time
      setup:
        gather_subset: "!all"
      delegate_to: "localhost"
      become: no
    - name: Set the date
      set_fact:
        mydate: "{{ ansible_date_time['epoch'] }}"
      delegate_to: "localhost"
      become: no
  roles:
# We use roles to load the vaults so that variables can be overwriten as they will have the second lowest priority.
    - vault_smtp
    - vault_mysql
    - role: get_network_information
      vars:
        mycommand: 'show ip route'
        mygroup: routers
        info_type: routing-table
        storage_server:  "{{ groups['backup_servers'][0] }}"
    - role: compare_and_alert
      vars:
        itm_loc: "/var/www/html/{{ info_type }}"
        itm_file_type: "{{ info_type }}"
```

License
-------
GPLv3


Author Information
------------------
Raul Mahiques <rmahique@redhat.com>
