manage_vlans
=========

Creation and deletion of VLANs

Requirements
------------

none

Role Variables
--------------

INPUT:
- vlan_id:
  This indicates the VLAN ID.

- vlan_name:
  This indicates the VLAN name.

- vlan_interfaces:
  This is a list of interfaces the VLAN will be assigned to.

Dependencies
------------

none


Example Playbook
----------------

```YAML
---
- name: Manage VLANs
  hosts: all
  connection: network_cli
  gather_facts: no
  roles:
    - manage_vlans
```

License
-------

GPLv3

Author Information
------------------

Raul Mahiques
